<?php
use DrewM\Drip\Drip;
use DrewM\Drip\Dataset;

/**
 * Process landing form submission.
 */
function process_landing_form_submission() {
    $email = $_POST['email'];
    $errors = new WP_Error();

    if (empty($email)) {
        $errors->add('email_empty', 'Please enter your email address');
    } else if (! is_email($email)) {
        $errors->add('email_invalid', 'That email address is not valid');
    }

    if (!$errors->get_error_codes()) {
        $email = sanitize_email($email);

        // add subscriber to Drip
        $Drip = new Drip(DRIP_API_TOKEN, DRIP_ACCOUNT_ID);
        $subscriber = new Dataset('subscribers', [
            'email' => $email
        ]);
        $Drip->post('subscribers', $subscriber);

        $event = new Dataset('events', [
            'email' => $email,
            'action' => 'Submitted lead magnet form',
            'properties' => [
                'name' => $_POST['form_name']
            ]
        ]);
        $Drip->post('events', $event);

        $response['data'] = $_POST;
    } else {
        http_response_code(422);
        $response['errors'] = $errors;
    }

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_process_landing_form_submission', 'process_landing_form_submission');
add_action('wp_ajax_nopriv_process_landing_form_submission', 'process_landing_form_submission');
