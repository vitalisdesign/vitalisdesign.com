<?php
use DrewM\Drip\Drip;
use DrewM\Drip\Dataset;

/**
 * Register custom post type.
 */
add_action('init', function() {
    register_post_type('service',
        [
          'labels' => [
            'name' => __('Services'),
            'singular_name' => __('Service'),
            'add_new_item' => __('Add New Service'),
            'edit_item' => __('Edit Service'),
            'search_items' => __('Search Services'),
            'not_found' => __('No services found.')
          ],
          'public' => true,
          'hierarchical' => true,
          'menu_icon' => 'dashicons-portfolio',
          'menu_position' => 20,
          'rewrite' => ['slug' => 'services'],
          'supports' => ['title']
        ]
    );
});

/**
 * Process contact form submission.
 */
function process_service_payment() {
    $email = $_POST['email'];
    \Stripe\Stripe::setApiKey(STRIPE_SECRET);

    try {
        $charge = \Stripe\Charge::create([
          'amount' => $_POST['amount'],
          'currency' => 'usd',
          'source' => $_POST['tokenId']
        ]);

        // record custom event in Drip
        $Drip = new Drip(DRIP_API_TOKEN, DRIP_ACCOUNT_ID);
        $subscriber = new Dataset('subscribers', [
          'email' => $email
        ]);
        $Drip->post('subscribers', $subscriber);

        $event = new Dataset('events', [
          'email' => $email,
          'action' => 'Purchased website audit'
        ]);
        $Drip->post('events', $event);
        $response['email'] = $email;
    } catch (\Stripe\Error\Base $e) {
        http_response_code(422);
        $response['error'] = $e->jsonBody['error']['message'];
    }

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_process_service_payment', 'process_service_payment');
add_action('wp_ajax_nopriv_process_service_payment', 'process_service_payment');
