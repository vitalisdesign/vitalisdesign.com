<?php
/**
 * Register custom post type.
 */
add_action('init', function() {
    register_post_type('case-study',
        [
          'labels' => [
            'name' => __('Case Studies'),
            'singular_name' => __('Case Study'),
            'add_new_item' => __('Add New Case Study'),
            'edit_item' => __('Edit Case Study'),
            'search_items' => __('Search Case Studies'),
            'not_found' => __('No case studies found.')
          ],
          'public' => true,
          'hierarchical' => true,
          'menu_icon' => 'dashicons-index-card',
          'menu_position' => 21,
          'rewrite' => ['slug' => 'case-studies'],
          'supports' => ['title', 'thumbnail']
        ]
    );
});
