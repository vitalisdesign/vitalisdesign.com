<?php
use DrewM\Drip\Drip;
use DrewM\Drip\Dataset;

/**
 * Process contact form submission.
 */
function process_contact_form_submission() {
    $name = sanitize_text_field($_POST['name']);
    $email = $_POST['email'];
    $company = sanitize_text_field($_POST['company']);
    $website = sanitize_text_field($_POST['website']);
    $message = sanitize_text_field($_POST['message']);

    $errors = new WP_Error();

    if (empty($email)) {
        $errors->add('email_empty', 'Please enter your email address', ['form_field' => 'email']);
    } else if (! is_email($email)) {
        $errors->add('email_invalid', 'That email address is not valid', ['form_field' => 'email']);
    }

    if (empty($name)) {
        $errors->add('name_empty', 'Please enter your name', ['form_field' => 'name']);
    }

    if (empty($message)) {
        $errors->add('message_empty', 'Please tell me briefly about the project', ['form_field' => 'message']);
    }

    if (!$errors->get_error_codes()) {
        $email = sanitize_email($email);
        $message = wp_unslash($message);

        $parts = explode(' ', $name);
        $last_name = array_pop($parts);
        $first_name = implode(' ', $parts);
        $custom_fields = [
            'first_name' => $first_name,
            'last_name' => $last_name
        ];

        if (!empty($company)) {
            $custom_fields['company'] = $company;
        }

        if (!empty($website)) {
            $custom_fields['website'] = $website;
        }

        // add subscriber to Drip
        $Drip = new Drip(DRIP_API_TOKEN, DRIP_ACCOUNT_ID);
        $subscriber = new Dataset('subscribers', [
            'email' => $email,
            'custom_fields' => $custom_fields
        ]);
        $Drip->post('subscribers', $subscriber);

        $event = new Dataset('events', [
            'email' => $email,
            'action' => 'Submitted contact form',
            'properties' => [
                'message' => $message
            ]
        ]);
        $Drip->post('events', $event);

        // send notification email to admin
        $to = get_bloginfo('admin_email');
        $subject = 'You have a new contact form submission from ' . get_bloginfo('name');
        $headers = [
            'Content-Type: text/html; charset=UTF-8'
        ];

        ob_start(); ?>
        Name: <strong><?= $name; ?></strong><br />
        Email address: <a href="mailto:<?= $email; ?>"><?= $email; ?></a><br />
        Company: <?= $company; ?><br />
        Current website: <a href="<?= $website; ?>" target="_blank"><?= $website; ?></a><br /><br />

        Message:<br />
        <em><?= $message; ?></em><br /><br />

        <?php $message = ob_get_clean();

        wp_mail($to, $subject, $message, $headers);

        $response = [
            'message' => 'Thanks for your message! I\'ll be in touch soon.'
        ];
    } else {
        $error_codes = $errors->get_error_codes();
        $form_errors = [];
        foreach ($error_codes as $error_code) {
            $error_data = $errors->get_error_data($error_code);
            $form_errors[$error_data['form_field']] = $errors->get_error_message($error_code);
        }

        http_response_code(422);
        $response['errors'] = $form_errors;
    }

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_process_contact_form_submission', 'process_contact_form_submission');
add_action('wp_ajax_nopriv_process_contact_form_submission', 'process_contact_form_submission');
