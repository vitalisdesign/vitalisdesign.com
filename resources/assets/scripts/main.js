// import external dependencies
import 'jquery';

// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import contact from './routes/contact';
import websiteAudit from './routes/services-website-audit';

/** Populate Router instance with DOM routes */
const routes = new Router({
  common,
  home,
  contact,
  websiteAudit
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
