/*global ajax, dataLayer, fbq */

export default {
  init() {
    contactForm()
  }
}

/**
 * Submit contact form via AJAX.
 */
function contactForm() {
  jQuery('.contact__form').submit(function() {
    let form = jQuery(this)
    let data = form.serializeArray()
    data.push({
      'name': 'action',
      'value': 'process_contact_form_submission'
    })

    jQuery.ajax({
      type: 'POST',
      url: ajax.url,
      data: jQuery.param(data),
      dataType: 'json',
      beforeSend: function() {
        jQuery(form).find('button[type=submit]').attr('disabled', true)
      },
      success: function(response) {
        jQuery('.error').removeClass('error')
        jQuery('.field-error').remove()

        if (typeof dataLayer !== 'undefined') {
          dataLayer.push({'event': 'submitted_contact_form'})
        }

        if (typeof fbq !== 'undefined') {
          fbq('track', 'Lead')
        }

        jQuery(form).children().not('.user-feedback').slideUp(150)
        jQuery(form).prepend('<div class="user-feedback success">' + response.message + '</div>').hide().fadeIn(300)
      },
      error: function(response) {
        jQuery('.error').removeClass('error')
        jQuery('.field-error').remove()
        const errors = response.responseJSON.errors

        for (const key in errors) {
          if (errors.hasOwnProperty(key)) {
            jQuery(form).find('#' + key).parent().addClass('error').append('<div class="field-error">' + errors[key] + '</div>')
          }
        }

        jQuery(form).find('button[type=submit]').removeAttr('disabled')
      },
    })
    return false
  })

  jQuery('input[type=url]').change(function() {
    if (!/^https*:\/\//.test(this.value)) {
      this.value = 'http://' + this.value
    }
  })
}
