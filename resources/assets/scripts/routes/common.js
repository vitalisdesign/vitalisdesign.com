/*global ajax, dataLayer, fbq */

export default {
  init() {
    headerToggle()
    headerScroll()
    landingForm()
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  }
}

let header = jQuery('.header')
let headerSpacer = jQuery('.header__spacer')

function headerToggle() {
  jQuery('.header__toggle').click(function() {
    jQuery(this).toggleClass('header__toggle--open')
    jQuery(header).toggleClass('header--open')
    jQuery('.header__spacer').toggleClass('header-menu--open')
  })

  jQuery(document).on('click touchstart', function(e) {
    if (! jQuery(e.target).closest(header).length) {
      jQuery(header).removeClass('header--open')
      jQuery('.header__toggle').removeClass('header__toggle--open')
      jQuery('.header__spacer').removeClass('header-menu--open')
    }
  })
}

function headerScroll() {
  jQuery(window).on('scroll', function() {
    if (jQuery(window).scrollTop() > jQuery(headerSpacer).outerHeight()) {
      jQuery(header).addClass('header--fixed')
      jQuery('.header__spacer-nav').show()
    } else {
      jQuery(header).removeClass('header--fixed')
      jQuery('.header__spacer-nav').hide()
    }
  })
}

/**
 * Submit landing page form(s) via AJAX.
 */
function landingForm() {
  jQuery('.landing__form').submit(function() {
    let form = jQuery(this)
    let data = form.serializeArray()
    data.push({
      'name': 'action',
      'value': 'process_landing_form_submission'
    })

    jQuery.ajax({
      type: 'POST',
      url: ajax.url,
      data: jQuery.param(data),
      dataType: 'json',
      beforeSend: function() {
        jQuery(form).find('button[type=submit]').attr('disabled', true)
      },
      success: function(response) {
        if (typeof dataLayer !== 'undefined') {
          dataLayer.push({'event': 'downloaded_lead_magnet'})
        }

        if (typeof fbq !== 'undefined') {
          fbq('track', 'Lead')
        }

        window.location.href = response.data.redirect_to
      },
      error: function() {
        jQuery(form).find('input[type=email]').addClass('error')
        jQuery(form).find('button[type=submit]').removeAttr('disabled')
      },
    })
    return false
  })
}
