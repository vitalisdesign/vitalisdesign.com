/*global StripeCheckout, ajax, dataLayer */

export default {
  init() {
    let handler = StripeCheckout.configure({
      key: ajax.stripeKey,
      locale: 'auto',
      zipCode: true,
      token: function(token) {
        let data = {
          action: 'process_service_payment',
          tokenId: token.id,
          email: token.email,
          amount: jQuery('.stripe-checkout').data('amount')
        }
        jQuery.ajax({
          type: 'POST',
          url: ajax.url,
          data: data,
          dataType: 'json',
          beforeSend: function() {
            jQuery('.ui-loader').addClass('ui-loader--active')
            jQuery('.service__cta .error').remove()
          },
          success: function(response) {
            if (typeof dataLayer !== 'undefined') {
              dataLayer.push({'event': 'purchased_website_audit'})
            }

            window.location.href = 'https://philiplindberg.typeform.com/to/MtB7GU?email=' + response.email
          },
          error: function(response) {
            jQuery('.ui-loader').removeClass('ui-loader--active')
            jQuery('.service__cta').append('<div class="error">' + response.responseJSON.error + '</div>')
          }
        })
      }
    })

    jQuery('.stripe-checkout').click(function(e) {
      handler.open({
        image: ajax.contentUrl + '/uploads/2018/01/social_media_icon.png',
        name: jQuery(this).data('name'),
        description: jQuery(this).data('description'),
        amount: jQuery(this).data('amount')
      })
      e.preventDefault()
    })

    window.addEventListener('popstate', function() {
      handler.close()
    })
  }
}
