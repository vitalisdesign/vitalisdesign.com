<?php
$price = (float) str_replace(',', '', get_field('excerpt_price'));
$discount = false;

if (!empty($_GET['discount']) && $_GET['discount'] == '20%') {
  $discounted_price = $price - $price * 0.2;
  $discount = true;
}

$cta = get_field('cta');
ob_start(); ?>
<div class="service__cta">
  <?php if (get_field('payment')) { ?>
    <button
      class="stripe-checkout ui-button ui-button--primary"
      data-name="<?= get_the_title(); ?>"
      data-description="By Vitalis Design"
      data-amount="<?= $discount ? $discounted_price * 100 : $price * 100; ?>">
      <?= $cta['text']; ?>
    </button>

    <?php if ($discount) { ?>
      <div class="discount">
        <div class="discount__heading">20% off discount applied!</div>
        <div class="discount__price">
          Your price: <span class="old"><?= '$' . number_format($price); ?></span> <span class="new"><?= '$' . number_format($discounted_price); ?></span>
        </div>
      </div>
    <?php } ?>
  <?php } else { ?>
    <a class="ui-button ui-button--primary" href="<?= $cta['link']; ?>"><?= $cta['text']; ?></a>
  <?php } ?>
</div>
<?php $cta_element = ob_get_clean();
?>

<div class="service">
  <h1><?= get_the_title(); ?></h1>
  <h2><?= get_field('description'); ?></h2>

  <?= $cta_element; ?>

  <div class="service__body"><?= get_field('body'); ?></div>

  <?= $cta_element; ?>
</div>
