<div class="case-study">
  <h1><?= get_the_title(); ?></h1>
  <h2><?= get_field('tagline'); ?></h2>

  <a class="case-study__link ui-button ui-button--primary" href="<?= get_field('link'); ?>" target="_blank">Visit website</a>

  <div class="section__divider section__divider--heading">Challenge</div>

  <div class="case-study__challenge"><?= get_field('challenge'); ?></div>

  <div class="section__divider section__divider--heading">Objective</div>

  <div class="case-study__objective"><?= get_field('objective'); ?></div>

  <div class="section__divider section__divider--heading">The Process</div>

  <?php while (have_rows('details')) : the_row(); ?>
    <section class="case-study--<?= get_row_layout(); ?> case-study__section">
      <?php
      switch (get_row_layout()) {
        case 'centered_content':
          echo get_sub_field('content');
          break;
        case 'content_2_columns':
          ?>
          <div class="case-study--column-copy"><?= get_sub_field('column_copy'); ?></div>
          <div class="case-study--column-media"><?= get_sub_field('column_media'); ?></div>
          <?php
          break;
      }
      ?>
    </section>
  <?php endwhile; ?>

  <?php if (have_rows('results')) { ?>
    <div class="section__divider section__divider--heading">Results</div>

    <div class="case-study__results">
      <?php while (have_rows('results')) : the_row(); ?>
        <div class="case-study__results__item">
          <div class="case-study__results__value"><?= get_sub_field('value'); ?></div>
          <div class="case-study__results__metric"><?= get_sub_field('metric'); ?></div>
        </div>
      <?php endwhile; ?>
    </div>
  <?php } ?>

  <?php if (get_field('testimonial_quote')) { ?>
    <div class="case-study__testimonial">
      <div class="case-study__testimonial__photo">
        <div class="case-study__testimonial__image">
          <img src="<?= get_field('testimonial_image')['sizes']['medium_large']; ?>" />
        </div>

        <div class="case-study__testimonial__author-name"><?= get_field('testimonial_author_name'); ?></div>
        <div class="case-study__testimonial__author-title"><?= get_field('testimonial_author_title'); ?></div>
      </div>

      <div class="case-study__testimonial__content">
        <i class="material-icons">format_quote</i>
        <div class="case-study__testimonial__quote"><?= get_field('testimonial_quote'); ?></div>
      </div>
    </div>
  <?php } ?>

  <?php
  if (get_next_post()) {
    $query = new WP_Query([
      'p' => get_next_post()->ID,
      'post_type' => 'case-study'
    ]);
  } else {
    $query = new WP_Query([
      'post_type' => 'case-study',
      'posts_per_page' => 1,
      'orderby' => 'menu_order',
      'order' => 'ASC'
    ]);
  }

  while ($query->have_posts()) : $query->the_post(); global $post;
    ?>
    <a class="case-study__next content__panel content__panel--hover" href="<?= get_permalink(); ?>">
      <div class="case-study__next__copy">
        <h3 class="case-study__next__heading"><span class="weaker">Next up - </span><?= get_the_title(); ?></h3>
        <div class="case-study__next__tagline"><?= get_field('tagline'); ?></div>
      </div>

      <div class="case-study__next__image" style="background-image: url(<?= get_the_post_thumbnail_url($post->ID, 'large'); ?>);"></div>
    </a>
    <?php
  endwhile; wp_reset_query();
  ?>
</div>
