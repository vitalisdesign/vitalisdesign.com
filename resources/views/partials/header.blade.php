<div class="header__spacer"></div>
<div class="header__spacer-nav"></div>

<header class="header">
  <div class="container">
    <a class="header__logo" href="{{ home_url('/') }}">
      <img src="@asset('images/vitalis_design_logo.svg')" />
    </a>

    <nav class="header__nav">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => '']) !!}
      @endif
    </nav>

    <a class="header__toggle">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </a>
  </div>
</header>

<div class="ui-loader">
  <div class="la-ball-scale-pulse la-dark la-2x">
    <div></div>
    <div></div>
  </div>
</div>
