<div class="landing__hero">
  <div class="landing__hero__copy">
    <h1 class="landing__hero__heading"><?= get_field('hero_heading'); ?></h1>

    <h2 class="landing__hero__subheading"><?= get_field('hero_subheading'); ?></h2>

    <form class="landing__form landing__form--hero">
      <fieldset>
        <label for="email" class="sr-only">Email address</label>
        <input type="email" name="email" id="email" placeholder="Your email address">
      </fieldset>

      <button type="submit" class="landing__form__button ui-button ui-button--primary">
        <?= get_field('cta_text'); ?>
      </button>

      <input type="hidden" name="form_name" value="Conversion Optimization Guide" />
      <input type="hidden" name="redirect_to" value="<?= get_field('cta_link'); ?>" />
    </form>
  </div>

  <div class="landing__hero__image"><img src="<?= get_field('hero_image')['url']; ?>" /></div>
</div>

<?php while (have_rows('body')) : the_row(); ?>
  <section class="landing__<?= get_row_layout(); ?> landing__section">
    <?php
    switch (get_row_layout()) {
      case 'bullet_list':
        ?>
        <h3><?= get_sub_field('heading'); ?></h3>
        <ul>
          <?php while (have_rows('list')) : the_row(); ?>
            <li><?= get_sub_field('item'); ?></li>
          <?php endwhile; ?>
        </ul>
        <?php
        break;
      case 'faq':
        ?>
        <div class="landing__faq__list">
          <?php while (have_rows('list')) : the_row(); ?>
            <div class="landing__faq__item">
              <h4><?= get_sub_field('question'); ?></h4>
              <p><?= get_sub_field('answer'); ?></p>
            </div>
          <?php endwhile; ?>
        </div>
        <?php
        break;
    }
    ?>
  </section>
<?php endwhile; ?>

<form class="landing__form landing__form--footer">
  <fieldset>
    <label for="email" class="sr-only">Email address</label>
    <input type="email" name="email" id="email" placeholder="Your email address">
  </fieldset>

  <button type="submit" class="landing__form__button ui-button ui-button--primary">
    <?= get_field('cta_text'); ?>
  </button>
</form>
