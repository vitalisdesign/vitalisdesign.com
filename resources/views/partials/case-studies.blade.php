<div class="case-studies-index">
  <?php
  $query = new WP_Query([
    'post_type' => 'case-study',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
  ]);

  while ($query->have_posts()) : $query->the_post(); global $post;
    ?>
    <a class="case-studies-index__item" href="<?= get_permalink(); ?>">
      <div class="case-studies-index__image" style="background-image: url(<?= get_the_post_thumbnail_url($post->ID, 'max'); ?>);"></div>

      <div class="case-studies-index__content">
        <h2 class="case-studies-index__heading"><?= get_the_title(); ?></h2>

        <h3 class="case-studies-index__tagline"><?= get_field('tagline'); ?></h3>
      </div>
    </a>
    <?php
  endwhile; wp_reset_query();
  ?>
</div>

<a class="page-footer__cta section__divider" href="<?= get_permalink(get_page_by_path('contact')); ?>">Let's work together</a>
