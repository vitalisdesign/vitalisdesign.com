<div class="contact__content">
  <div class="contact__copy"><?= get_field('copy'); ?></div>

  <form class="contact__form" novalidate>
    <div class="contact__form__text-fields">
      <fieldset>
        <?php $namePlaceholder = 'Name'; ?>
        <label for="name" class="sr-only"><?= $namePlaceholder; ?></label>
        <input type="text" id="name" name="name" placeholder="<?= $namePlaceholder; ?>" />
      </fieldset>

      <fieldset>
        <?php $emailPlaceholder = 'Email address'; ?>
        <label for="email" class="sr-only"><?= $emailPlaceholder; ?></label>
        <input type="email" id="email" name="email" placeholder="<?= $emailPlaceholder; ?>" />
      </fieldset>
    </div>
    <div class="contact__form__text-fields">
      <fieldset>
        <?php $companyPlaceholder = 'Company'; ?>
        <label for="company" class="sr-only"><?= $companyPlaceholder; ?></label>
        <input type="text" id="company" name="company" placeholder="<?= $companyPlaceholder; ?>" />
      </fieldset>

      <fieldset>
        <?php $websitePlaceholder = 'Current website'; ?>
        <label for="website" class="sr-only"><?= $websitePlaceholder; ?></label>
        <input type="url" id="website" name="website" placeholder="<?= $websitePlaceholder; ?>" />
      </fieldset>
    </div>
    <fieldset>
      <?php $messagePlaceholder = 'Tell me a bit about the project. What business goals do you want to achieve?'; ?>
      <label for="message" class="sr-only"><?= $messagePlaceholder; ?></label>
      <textarea id="message" name="message" placeholder="<?= $messagePlaceholder; ?>" rows="6"></textarea>
    </fieldset>

    <button class="contact__form__submit ui-button ui-button--primary" type="submit"  value="">Let's talk</button>
  </form>
</div>

<div class="contact__map map" id="contact-map"></div>

<script>
function initMap() {
  var markerPosition = {
    lat: <?= get_field('map')['address']['lat']; ?>,
    lng: <?= get_field('map')['address']['lng']; ?>
  };
  var map = new google.maps.Map(document.getElementById('contact-map'), {
    center: markerPosition,
    zoom: 12,
    disableDefaultUI: true,
    zoomControl: true,
    styles: [
      {
        "featureType": "all",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "saturation": 36
          },
          {
            "color": "#333333"
          },
          {
            "lightness": 40
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#ffffff"
          },
          {
            "lightness": 16
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#fefefe"
          },
          {
            "lightness": 20
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#fefefe"
          },
          {
            "lightness": 17
          },
          {
            "weight": 1.2
          }
        ]
      },
      {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "simplified"
          }
        ]
      },
      {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          },
          {
            "lightness": 20
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          },
          {
            "lightness": 21
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "lightness": 21
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 17
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 29
          },
          {
            "weight": 0.2
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 18
          },
          {
            "visibility": "simplified"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 16
          },
          {
            "visibility": "simplified"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f2f2f2"
          },
          {
            "lightness": 19
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e9e9e9"
          },
          {
            "lightness": 17
          }
        ]
      }
    ]
  });

  let icon = {
    path: 'M11 2c-3.9 0-7 3.1-7 7 0 5.3 7 13 7 13 0 0 7-7.7 7-13 0-3.9-3.1-7-7-7Zm0 9.5c-1.4 0-2.5-1.1-2.5-2.5 0-1.4 1.1-2.5 2.5-2.5 1.4 0 2.5 1.1 2.5 2.5 0 1.4-1.1 2.5-2.5 2.5Z',
    fillColor: '#000000',
    fillOpacity: 0.8,
    anchor: new google.maps.Point(10,20),
    strokeWeight: 0,
    scale: 1.6
  }

  var marker = new google.maps.Marker({
    position: markerPosition,
    map: map,
    icon: icon,
    animation: google.maps.Animation.DROP
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  var infowindow = new google.maps.InfoWindow({
    content: '<div class="line-1"><?= get_field('map')['info_window']['line_1']; ?></div>' +
      '<div class="line-2"><?= get_field('map')['info_window']['line_2']; ?></div>'
  });
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY; ?>&callback=initMap"
    async defer></script>
