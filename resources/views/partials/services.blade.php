<div class="services-list">
  <?php
  $query = new WP_Query([
    'post_type' => 'service',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
  ]);

  while ($query->have_posts()) : $query->the_post();
    $excerpt = get_field('excerpt');
    ?>
    <a class="services-list__item content__panel content__panel--hover" href="<?= get_permalink(); ?>">
      <div class="services-list__item__copy">
        <div class="services-list__item__heading">
          <h3><?= get_the_title(); ?></h3>
          <div class="services-list__item__price">$<?= $excerpt['price']; ?></div>
        </div>

        <div class="services-list__item__body"><?= $excerpt['copy']; ?></div>
      </div>

      <div class="services-list__item__link ui-button ui-button--default ui-button--no-hover">Learn more</div>
    </a>
    <?php
  endwhile; wp_reset_query();
  ?>

  <a class="services-list__item content__panel content__panel--hover" href="<?= get_permalink(get_page_by_path('landing/conversion-optimization-guide')); ?>">
    <div class="services-list__item__copy">
      <div class="services-list__item__heading">
        <h3>Not ready for a big investment?</h3>
      </div>

      <div class="services-list__item__body">
        <p>No worries! Click on the button below to receive my free guide, <strong>“Why your website doesn’t convert and what to do about it”</strong>.</p>

        <p>In this free guide, you’ll learn how to effectively convert visitors into customers by crafting compelling value propositions, driving targeted traffic that actually converts and overcoming customer objections.</p>
      </div>
    </div>

    <div class="services-list__item__link ui-button ui-button--default ui-button--no-hover">Get the free guide</div>
  </a>
</div>
