<?php
$hero = get_field('hero');
$client_logos = get_field('client_logos');
$benefits = get_field('benefits');
$lead_magnet = get_field('lead_magnet');
?>

<div class="home__hero">
  <h1 class="home__hero__heading"><?= $hero['heading']; ?></h1>
  <h2 class="home__hero__subheading"><?= $hero['subheading']; ?></h2>

  <div class="home__hero__buttons">
    <a class="ui-button ui-button--primary" href="<?= get_permalink(get_page_by_path('contact')); ?>">Let's talk</a>
    <a class="ui-button ui-button--default" href="<?= get_permalink(get_page_by_path('services')); ?>">My services</a>
  </div>
</div>

<div class="section__divider section__divider--heading section__divider--smaller"><?= $client_logos['heading']; ?></div>

<div class="home__client-logos">
  <?php while(have_rows('client_logos_items')) : the_row(); ?>
    <a class="home__client-logos__item" href="<?= get_sub_field('link'); ?>" target="_blank">
      <img src="<?= get_sub_field('logo')['url']; ?>" />
    </a>
  <?php endwhile; ?>
</div>

<div class="home__intro"><?= get_field('intro'); ?></div>

<div class="home__benefits__divider section__divider"><?= $benefits['heading']; ?></div>

<div class="home__benefits">
  <?php while(have_rows('benefits_items')) : the_row(); ?>
    <div class="home__benefits__item">
      <div class="home__benefits__copy">
        <h3><?= get_sub_field('heading'); ?></h3>
        <?= get_sub_field('body'); ?>
      </div>

      <div class="home__benefits__image">
        <img src="<?= get_sub_field('image')['url']; ?>" />
      </div>
    </div>
  <?php endwhile; ?>
</div>

<a class="home__lead-magnet content__panel content__panel--hover" href="<?= $lead_magnet['cta']['link']; ?>">
  <div class="home__lead-magnet__copy">
    <h3><?= $lead_magnet['heading']; ?></h3>
    <?= $lead_magnet['body']; ?>

    <div class="home__lead-magnet__cta ui-button ui-button--default"><?= $lead_magnet['cta']['text']; ?></div>
  </div>

  <div class="home__lead-magnet__image">
    <img src="<?= $lead_magnet['image']['url']; ?>" />
  </div>
</a>
