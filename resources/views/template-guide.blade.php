{{--
  Template Name: Guide
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    <div class="guide">
      @include('partials.page-header')
      @include('partials.guide')
    </div>
  @endwhile
@endsection
