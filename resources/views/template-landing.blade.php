{{--
  Template Name: Landing Page
--}}

@extends('layouts.landing')

@section('content')
  @while(have_posts()) @php(the_post())
    <div class="landing">
      @include('partials.landing')
    </div>
  @endwhile
@endsection
